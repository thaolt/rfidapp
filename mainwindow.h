#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    static MainWindow & instance() {
        static MainWindow * _instance = 0;
        if ( _instance == 0 ) {
            _instance = new MainWindow();
        }
        return *_instance;
    }

public slots:
    void addScannedCard(QString);

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
