#ifndef RFIDREADER_H
#define RFIDREADER_H

#include <QObject>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QFuture>

class RFIDReader : public QObject
{
    Q_OBJECT
public:
    explicit RFIDReader(QObject *parent = nullptr);
    static RFIDReader & instance() {
        static RFIDReader * _instance = 0;
        if ( _instance == 0 ) {
            _instance = new RFIDReader();
        }
        return *_instance;
    }

public slots:
    void startScan();
    void cleanUp();
    void onReadyRead();

signals:
    void cardScanned(QString);

    void portConnected(QString portName);
    void portDisconnected();

private:
    static const int vendorId = 0x0403;  // FTDI FT232RL
//    static const int vendorId = 0x2341;  // Arduino 32U4
    bool m_abort = false;
    bool m_connected = false;

    QSerialPort *m_serial = nullptr;
    QByteArray m_serialData;
    QString m_serialBuffer;
    QFuture<void> m_scanHandle;

    int m_flag; //data stream

private slots:
    void doScan();
    void onAboutToClose();
    void onDeviceConnected(QString portName);
    void onDeviceDisconnected();
};

#endif // RFIDREADER_H
