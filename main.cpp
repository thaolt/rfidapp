#include "mainwindow.h"
#include "scanningform.h"
#include "rfidreader.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow *w = &MainWindow::instance();
    ScanningForm *sf = &ScanningForm::instance();
    RFIDReader *reader = &RFIDReader::instance();

    sf->show();
    reader->startScan();

    a.connect(reader, &RFIDReader::portConnected, w, &MainWindow::show);
    a.connect(reader, &RFIDReader::portConnected, sf, &ScanningForm::hide);

    a.connect(reader, &RFIDReader::cardScanned, w, &MainWindow::addScannedCard);

    a.connect(reader, &RFIDReader::portDisconnected, sf, &ScanningForm::show);
    a.connect(reader, &RFIDReader::portDisconnected, w, &MainWindow::hide);

    return a.exec();
}
