#include "rfidreader.h"
#include <QThread>
#include <QtConcurrent>
#include <QApplication>

RFIDReader::RFIDReader(QObject *parent) : QObject(parent)
{
    m_serial = new QSerialPort();
    m_serial->setBaudRate(QSerialPort::Baud115200);
    m_serial->setDataBits(QSerialPort::Data8);
    m_serial->setFlowControl(QSerialPort::NoFlowControl);
    m_serial->setParity(QSerialPort::NoParity);
    m_flag = 0;
    connect(m_serial, &QSerialPort::aboutToClose, this, &RFIDReader::onAboutToClose);
    connect(m_serial, &QSerialPort::readyRead, this, &RFIDReader::onReadyRead);

    connect(this, &RFIDReader::portConnected, this, &RFIDReader::onDeviceConnected);
    connect(this, &RFIDReader::portDisconnected, this, &RFIDReader::onDeviceDisconnected);

    connect(QApplication::instance(), &QApplication::aboutToQuit, this, &RFIDReader::cleanUp);
}

void RFIDReader::startScan()
{
    this->m_scanHandle = QtConcurrent::run(this, &RFIDReader::doScan);
}

void RFIDReader::doScan()
{
    // separated thread
    QString portName;
    while (!m_abort) {
        bool found = false;
        // SERIAL port scanning
        // qDebug() << "Number of available port:" << QSerialPortInfo::availablePorts().length();

        foreach (const QSerialPortInfo &serialPortInfo, QSerialPortInfo::availablePorts()) {
            if(serialPortInfo.hasVendorIdentifier() && serialPortInfo.vendorIdentifier() == this->vendorId) {
                portName = serialPortInfo.portName();
                found = true;
            }
        }
        if (!m_connected && found) {
            emit portConnected(portName);
            qDebug() << "Port connect " << portName;
            m_connected = true;
        }

        if (m_connected && !found) {
            emit portDisconnected();
            m_connected = false;
        }
        QThread::sleep(1);
    }
}

void RFIDReader::onReadyRead()
{
    const QByteArray data = m_serial->readAll();
    foreach (char c, data) {
        if (c != 10) {
            this->m_serialBuffer.append(c);
        }  else {
            QString cardId = m_serialBuffer.split(":").last();
            cardId = cardId.trimmed();
            cardId = cardId.remove(' ');
            m_serialBuffer.clear();
            emit cardScanned(cardId);
        }
    }
}

void RFIDReader::onAboutToClose()
{
    qDebug() << "Serial port is about to close";
}

void RFIDReader::onDeviceConnected(QString portName)
{
    qDebug() << "Found serial port";
    m_serial->setPortName(portName);
    m_serial->open(QSerialPort::ReadWrite);
    m_serial->write("startup\n", 8);
}

void RFIDReader::onDeviceDisconnected()
{
    qDebug() << "Serial port disconnected";
    m_serial->close();
}

void RFIDReader::cleanUp()
{
    m_abort = true;
}
