#ifndef SCANNINGFORM_H
#define SCANNINGFORM_H

#include <QWidget>

namespace Ui {
class ScanningForm;
}

class ScanningForm : public QWidget
{
    Q_OBJECT

public:
    explicit ScanningForm(QWidget *parent = nullptr);
    ~ScanningForm();

    static ScanningForm & instance() {
        static ScanningForm * _instance = 0;
        if ( _instance == 0 ) {
            _instance = new ScanningForm();
        }
        return *_instance;
    }

private:
    Ui::ScanningForm *ui;
};

#endif // SCANNINGFORM_H
